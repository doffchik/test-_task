from rest_framework import serializers
from .models import Post, Like, Unlike


class PostListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'


class PostCreateSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Post
        exclude = ['date_creation']


class LikesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = '__all__'


class UnlikesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unlike
        fields = '__all__'
