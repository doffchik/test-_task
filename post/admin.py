from django.contrib import admin
from .models import Like, Unlike, Post

class LikeAdmin(admin.ModelAdmin):
    fields = ['user', 'post']
    readonly_fields= ['user', 'post']

    class Meta:
        model = Like

class UnlikeAdmin(admin.ModelAdmin):
    class LikeAdmin(admin.ModelAdmin):
        fields = ['user', 'post']
        readonly_fields = ['user', 'post']

        class Meta:
            model = Unlike


admin.site.register(Like, LikeAdmin)
admin.site.register(Unlike, UnlikeAdmin)
admin.site.register(Post)
