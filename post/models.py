import random
from datetime import datetime

from django.db import models
from api_auth.models import User

from django.utils.timezone import make_aware


class Post(models.Model):

    title = models.CharField(max_length=255, unique=True)
    text = models.TextField(null=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    date_creation = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'Post'


class Like(models.Model):

    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='likes')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')
    date_creation = models.DateField(auto_now_add=True)

    def change_to_random_date(self):
        day_range = range(1,30)
        self.date_creation = make_aware(datetime(2021, 5, random.choice(day_range)))
        self.save()

    def create(self, post_id, user_id):
        self.post_id = post_id
        self.user_id = user_id
        self.save()
        return self

    class Meta:
        db_table = 'Like'


class Unlike(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='unlikes')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='unlikes')
    date_creation = models.DateField(auto_now_add=True)

    def change_to_random_date(self):
        day_range = range(1,30)
        self.date_creation = make_aware(datetime(2021, 5, random.choice(day_range)))
        self.save()

    def create(self, post_id, user_id):
        self.post_id = post_id
        self.user_id = user_id
        self.save()
        return self

    class Meta:
        db_table = 'Unlike'