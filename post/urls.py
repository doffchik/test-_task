from django.urls import path
from . import views

urlpatterns = [
    path('', views.api_roots, name='api_roots'),

    path('posts/', views.PostsList.as_view(), name="posts_list"),
    path('create-post/', views.CreatePost.as_view(), name="create_post"),

    path('likes/', views.LikesList.as_view(), name="likes_list"),
    path('unlikes/', views.UnlikesList.as_view(), name="unlikes_list"),

    path('like-post/<int:post_id>', views.AddLikeForPost.as_view(), name='add_like_for_post'),
    path('unlike-post/<int:post_id>', views.AddUnlikeForPost.as_view(), name='add_unlike_for_post'),

    path('like-analytics/', views.LikeAnalytics.as_view(), name='like_analytics')
]
