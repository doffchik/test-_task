import random
from datetime import datetime
from django.utils.timezone import make_aware

from post.models import Like, Unlike


random_list_of_day = random.sample([2, 4, 5], 10)


for day in random_list_of_day:
    like = Like()
    like.create(post_id=1, user_id=1)
    like.date_creation = make_aware(datetime(2021, 5, day))
    like.save()
