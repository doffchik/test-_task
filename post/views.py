from rest_framework import generics, views
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response

from . import serializers as app_serializers
from . import services
from .models import Post, Like, Unlike


@api_view(['GET'])
def api_roots(request):
    current_host = request.get_host()
    return Response({
        'posts-list': reverse('posts_list', request=request),
        'create-post': reverse('create_post', request=request),
        'likes-list': reverse('likes_list', request=request),
        'unlikes-list': reverse('unlikes_list', request=request),
        'like-analytics': reverse('like_analytics', request=request)
    })


class PostsList(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = app_serializers.PostListSerializer


class CreatePost(generics.CreateAPIView):
    model = Post
    serializer_class = app_serializers.PostCreateSerializer
    permission_classes = [permissions.IsAuthenticated]


class LikesList(generics.ListAPIView):
    queryset = Like.objects.all()
    serializer_class = app_serializers.LikesListSerializer


class UnlikesList(generics.ListAPIView):
    queryset = Unlike.objects.all()
    serializer_class = app_serializers.UnlikesListSerializer


class AddLikeForPost(views.APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, post_id):
        created_like = services.LikeCreate(post_id, request.user.id)

        return Response(created_like.message)


class AddUnlikeForPost(views.APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, post_id):
        created_unlike = services.UnlikeCreate(post_id, request.user.id)
        return Response(created_unlike.message)


class LikeAnalytics(views.APIView):
    # https: // docs.djangoproject.com / en / 3.2 / topics / db / aggregation /

    def get(self, request):
        like_analyzer = services.LikeAnalyzer(
            date_from=request.GET.get('date_from'),
            date_to=request.GET.get('date_to'))
        return Response(like_analyzer.get_result())
