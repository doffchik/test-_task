from django.http import Http404
from django.db.models import Q, Count

from .models import Like, Unlike, Post


class CreateLikeMixin:

    model = None

    def __init__(self, post_id, user_id):
        self.post_id = post_id
        self.user_id = user_id

        self._validate_post_id()
        self._create_like()

    def _create_like(self):
        try:
            like = self.model().create(post_id=self.post_id, user_id=self.user_id)
            like.change_to_random_date()
            self.message = 'Like was successfully created'
        except Exception as error:
            self.message = error

    def _validate_post_id(self):
        if Post.objects.filter(id=self.post_id).count():
           return True
        raise Http404

    def _check_if_user_liked_this_post(self):
        if self.model.objects.filter(post_id=self.post_id, user_id=self.user_id):
            return 'This user already liked this post', 200
        else:
            return 'Like was successfully created', 201


class LikeCreate(CreateLikeMixin):
    model = Like


class UnlikeCreate(CreateLikeMixin):
    model = Unlike


class LikeAnalyzer:
    def __init__(self, date_from=None, date_to=None):
        self._date_from = date_from
        self._date_to = date_to

    def get_result(self):
        print()
        print(self.query_condition)
        print()
        return {
            'likes_count': Like.objects.filter(
                self.query_condition
            ).values('date_creation').order_by('date_creation').annotate(count_likes=Count('id')),

            'unlikes_count': Unlike.objects.filter(
                self.query_condition
            ).values('date_creation').order_by('date_creation').annotate(count_likes=Count('id')),
        }

    @property
    def query_condition(self):
        query_condition = Q()

        query_condition &= Q(date_creation__gte=self._date_from) if self._date_from else Q()
        query_condition &= Q(date_creation__lte=self._date_to) if self._date_to else Q()
        return query_condition

