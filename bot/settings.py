import os

BASE_DIR = os.path.dirname(__file__)
RULES_DATA = os.path.join(BASE_DIR, 'rules_data')

HOST_NAME = '127.0.0.1:8000'

SING_UP_URL = f'http://{HOST_NAME}/auth/sing-up/'
CREATE_TOKEN_URL = f'http://{HOST_NAME}/auth/token/'

POST_CREATE_URL = f'http://{HOST_NAME}/api/create-post/'

POST_TO_LIKE = 'http://127.0.0.1:8000/api/like-post/{post_id}'

LOGS_DIR = os.path.join(BASE_DIR, 'logs')

if not os.path.exists(LOGS_DIR):
    os.mkdir(LOGS_DIR)
