import time
import os
import json
import requests
import random

from loguru import logger
from datetime import date

import settings

logger.add(os.path.join(settings.LOGS_DIR, f'log({date.today().strftime("%d-%m-%y")}).log'),
    format='{time} {level} {module} {message}', compression='zip', rotation='00:00')


class Bot():

    def __init__(self, rules_file):
        """init method use _set_rules method to set all rules for bot
        :param rules_file - parameter is used for detecting parameters for inserting data"""

        self._rules = open(rules_file)

        self.users_info = None  # all inserted user data for storing to file

        self.count_of_users = None
        self.max_count_posts_per_user = None
        self.max_count_likes_per_user = None

        self._set_rules()

    def _set_rules(self):
        data = json.load(self._rules)
        self.count_of_users = data.get('count_of_users')
        self.max_count_posts_per_user = data.get('max_count_posts_per_user')
        self.max_count_likes_per_user = data.get('max_count_likes_per_user')

        if self.count_of_users is None or self.max_count_posts_per_user is None :
            raise AttributeError('Parameters: "count_of_users" or "max_count_posts_per_user" ' 
                                 'Cannot be None. Please set it in rules file')


    def _store_user_data(self):
        json_to_store = json.dumps(self.users_info, indent=4)
        open('result.json', 'w').write(json_to_store)

    def run(self):

        # first step - create users with tokens
        users_info = [
            self._get_access_to_api()
            for i in range(0, self.count_of_users)
        ]
        inserted_posts_ids = list()

        logger.info(f'Count created users: {len(users_info)}')

        # second step - create post for user
        for user_info in users_info:
            current_user_posts_id = list()
            _random_count = random.choice(range(1, self.max_count_posts_per_user + 1))
            logger.info(f'Start creating {_random_count} post for user')
            for post_number in range(_random_count):
                post_id = self._create_posts(token=user_info[0])
                current_user_posts_id.append(post_id)
                inserted_posts_ids.append(post_id)
                time.sleep(1)
            user_info.append({'posts_id':current_user_posts_id})

        self.users_info = users_info
        self._store_user_data()

        # third step - create likes for post
        for user_inf in users_info:
            for like_count in range(0, self.max_count_likes_per_user):
                self._create_likes_for_posts(
                    token=user_inf[0],
                    post_id=random.choice(list(set(inserted_posts_ids)))
                )


    @staticmethod
    def _get_access_to_api(fields_value='test'):

        fields_value = f'{fields_value}_{int(time.time())}'

        requests.post(
            url=settings.SING_UP_URL,
            data={'username': fields_value, 'password': fields_value}
        )
        response = requests.post(
            url=settings.CREATE_TOKEN_URL,
            data={'username': fields_value, 'password': fields_value}
        )

        try:
            access_data = json.loads(response.content.decode('utf-8'))
        except BaseException as error:
            logger.error("We can't get token")
            raise ValueError("We can't get token, check please url")

        if access_data.get('access'):
            return [access_data.get('access'), fields_value, fields_value]
        else:
            logger.warning(response.content)

    @staticmethod
    def _create_posts(token, fields_value='test'):

        fields_value = f'{fields_value}_{int(time.time())}'

        response = requests.post(
            url=settings.POST_CREATE_URL,
            data={'title': fields_value, 'text': fields_value},
            headers={'Authorization': f'Bearer {token}'}
        )
        if response.status_code == 201:
            post_id = json.loads(response.content.decode('utf-8')).get('id')
            logger.info(f'Wast created post. id - {post_id} ')
            return post_id
        else:
            logger.warning(response.content)

    @staticmethod
    def _create_likes_for_posts(token, post_id):

        logger.debug(settings.POST_TO_LIKE.format(post_id=post_id))

        response = requests.post(
            url=settings.POST_TO_LIKE.format(post_id=post_id),
            headers={'Authorization': f'Bearer {token}'}
        )
        if response.status_code == 201:
            logger.info(response.content)
        else:
            logger.warning(response.content)

if __name__ == '__main__':

    rules_file = os.path.join(settings.RULES_DATA, 'rules_1.json')

    bot = Bot(rules_file)
    bot.run()