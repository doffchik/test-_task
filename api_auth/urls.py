from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.auth_root, name='auth_root'),
    path('', include('djoser.urls')),
    path('', include('djoser.urls.jwt')),
    path('sing-up/', views.UserCreation.as_view(), name='sing_up'),

    path('my-information/', views.MyInformation.as_view(), name='my_information'),

    path('token/', views.LoginView.as_view(), name='get_token'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh_view'),
]
