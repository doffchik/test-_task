from rest_framework import serializers
from django.contrib.auth.hashers import make_password

from .models import User


class UserCreationSerializer(serializers.ModelSerializer):

    def validate_password(self, value: str) -> str:
        return make_password(value)

    class Meta:
        model = User
        fields = ['username', 'password']


class UserInformationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'last_login', 'last_request_date']
