from django.utils import timezone

class UserRequestMiddleware:
    def __init__(self, get_response):
        self._get_response = get_response

    def __call__(self, request):
        response = self._get_response(request)
        if request.user.is_authenticated:
            update_last_request_date(request.user)
        return response


def update_last_request_date(user):
    user.last_request_date = timezone.now()
    user.save()
