from django.apps import AppConfig
from django.db.models.signals import post_migrate

SUPER_USER_NAME = 'admin'
SUPER_USER_PASSWORD = 'admin'


class ApiAuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api_auth'

    def ready(self):
        post_migrate.connect(create_super_user, sender=self)


def create_super_user(sender, **kwargs):
    from api_auth.models import User

    try:
        User.objects.get(username=SUPER_USER_NAME)
        print(f'\nSuper user exists\n')
    except User.DoesNotExist:
        User.objects.create_superuser(username=SUPER_USER_NAME, email='', password=SUPER_USER_PASSWORD)
        print(f'Super: "{SUPER_USER_NAME}" user was successfully created')