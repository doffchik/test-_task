from django.contrib.auth import authenticate

from api_auth.models import User
from django.contrib.auth.models import update_last_login
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response

from .serializers import UserCreationSerializer, UserInformationSerializer


@api_view(['GET'])
def auth_root(request):
    return Response({
        'user-list':reverse('user-list', request=request),
        'sing_up': reverse('sing_up', request=request),
        'get_token': reverse('get_token', request=request),
        'token_refresh': reverse('token_refresh_view', request=request),
        'my_information': reverse('my_information', request=request)
    })


class LoginView(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        post_result = super().post(request, *args, **kwargs)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request=request, username=username, password=password)
        update_last_login(None, user)
        return post_result


class UserCreation(generics.CreateAPIView):

    model = User
    serializer_class = UserCreationSerializer


class MyInformation(APIView):

    def get(self, request):
        serializer = UserInformationSerializer(request.user)
        return Response(serializer.data)
