from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    last_request_date = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        db_table = 'User'
