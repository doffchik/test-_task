from django.http import HttpResponseRedirect
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET'])
def main_api_root(request):
    return Response({
        'auth-urls': f'http://{request.get_host()}/auth',
        'api-urls': f'http://{request.get_host()}/api',
    })
